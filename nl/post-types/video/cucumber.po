# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: cucumber\n"

msgid "Cucumber is part of the Cucurbitaceae family and belongs to the Cucumis sativus species. It is divided into five types:"
msgstr ""

msgid "open field cucumber whose skin is covered with small thorns,"
msgstr ""

msgid "greenhouse cucumbers with a very smooth skin,"
msgstr ""

msgid "'sikkim' type cucumbers with a red-orange skin,"
msgstr ""

msgid "cucumbers with very small fruit for processing, such as gerkins,"
msgstr ""

msgid "round-shaped cucumbers."
msgstr ""

msgid "The cucumber is a monoecious plant, meaning that it has both male and female flowers on the same plant. Female flowers have an ovary under the flower. It is in fact a mini cucumber that will develop after pollination. Male flowers are the first to appear and are at the end of long stems."
msgstr ""

msgid "The flowers only open during one day."
msgstr ""

msgid "Cucumbers can be self-fertilised, meaning that a female flower can be fertilised by pollen from a male flower of the same plant. But cross pollination is more common."
msgstr ""

msgid "Insects and mainly bees pollinate cucumber flowers."
msgstr ""

msgid "All varietes of the Cucumis sativus species cross-pollinate between each other. Cucumbers do not, however, cross with melons, watermelons or squashes."
msgstr ""

msgid "To avoid cross-pollination, keep a distance of 1 kilometer between two varieties. You can reduce this distance to 500 meters if there is a natural barrier such as a hedge."
msgstr ""

msgid "There are several methods to produce seeds from different varieties of cucumber grown in the same garden."
msgstr ""

msgid "One of them is to cover an entire variety with a net and to place a small bumble bee hive inside."
msgstr ""

msgid "Another is to cover two varieties in different nets : open one while the other is closed on one day, and alternate the next day. Let the wild insects do their work. The production will be smaller because certain flowers will not be pollinated."
msgstr ""

msgid "You can also pollinate the flowers manually. It is more delicate to do this with cucumbers than with squashes or zucchini as cucumber flowers are a lot smaller."
msgstr ""

msgid "These three methods are described in the modules on mechanical isolation techniques and on manual pollination in the “ABC of seed production”."
msgstr ""

msgid "Cucumbers grown for seed production are cultivated in the same way as those for consumption."
msgstr ""

msgid "Keep at least 6 plants to ensure good genetic diversity. Ideally, grow a dozen."
msgstr ""

msgid "Take great care to select the plants you keep for seeds in accordance with the specific characteristics of the variety. You should keep vigorous plants which have produced well-developed cucumbers. Get rid of sick plants."
msgstr ""

msgid "The maturity of cucumbers for seed production is not the same as for consumption. In fact, we generally eat unripe cucumbers."
msgstr ""

msgid "To produce seeds, it is important to let the cucumber develop until it is fully mature. It must have grown to its full size and its colour must have changed."
msgstr ""

msgid "You can also harvest the fruit shortly before maturity. In this case, place it in a warm area and let it ripen. This technique increases the fertility of the seeds."
msgstr ""

msgid "To extract the seeds, open the cucumber, remove the pulp with the seeds and let the mixture ferment for a few days. This fermentation will remove the viscous envelope that surrounds the seeds."
msgstr ""

msgid "For more information, refer to the module on wet-processing techniques in the ABC of seed production."
msgstr ""

msgid "You should then clean the seeds under running water using a sieve."
msgstr ""

msgid "To get rid of the empty sterile seeds, place all the seeds in a container full of water. The full, heavier seeds will sink to the bottom and the empty ones will float. Remove the empty ones, rince the good ones and dry them in a well-ventilated area. Rub them together to separate them."
msgstr ""

msgid "To be sure that the seeds are fully dry, they should break if you try to bend them."
msgstr ""

msgid "Write a label with the name of the species and variety as well as the year of harvesting and leave it inside the package, as writing on the outside may rub off."
msgstr ""

msgid "Leave the seeds in the freezer for a few days to kill any parasites."
msgstr ""

msgid "Cucumber seeds have a germination capacity of 6 years, sometimes longer. This can be prolonged by storing them in a freezer. One gram contains 30 to 40 seeds."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your cucumber seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own cucumber seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, cucumber, step by step, explanations"
msgstr ""
