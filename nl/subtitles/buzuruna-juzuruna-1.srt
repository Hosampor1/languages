﻿1
00:00:00,000 --> 00:00:05,780
(titel)

2
00:00:05,920 --> 00:00:08,760
-Wat betekent "Buzuruna Juzuruna"?

3
00:00:09,520 --> 00:00:12,100
Dat betekent "onze zaden, onze wortels".

4
00:00:12,140 --> 00:00:16,150
-Hoe verwoordt die naam wat jullie doen?

5
00:00:17,870 --> 00:00:25,170
Wij zijn een school/boerderij die zaadvast zaad produceert

6
00:00:26,090 --> 00:00:30,570
en we organiseren ook trainingen.

7
00:00:30,660 --> 00:00:35,520
Dat zijn onze twee belangrijkste activiteiten.

8
00:00:36,220 --> 00:00:39,350
We produceren dus zaden

9
00:00:39,530 --> 00:00:42,460
die op hun beurt wortels worden.

10
00:00:42,460 --> 00:00:47,020
Zo werken we aan autonome boerenlandbouw

11
00:00:47,130 --> 00:00:49,380
en voedselsoevereiniteit.

12
00:00:49,380 --> 00:00:53,120
Dat klinkt allemaal goed samen.

13
00:00:54,080 --> 00:01:04,730
-Waarom is voedingsautonomie vandaag zo belangrijk in Libanon?

14
00:01:05,600 --> 00:01:07,840
Het is hoogdringend.

15
00:01:07,860 --> 00:01:13,600
We beleven een nooit geziene economische crisis,

16
00:01:13,600 --> 00:01:15,600
die extreem hard is.

17
00:01:15,710 --> 00:01:24,150
Onze munteenheid heeft 10 maal haar waarde verloren.

18
00:01:24,260 --> 00:01:30,020
We zijn van 1500 Libanese ponden per dollar naar 15.000 gezakt op een gegeven moment

19
00:01:30,040 --> 00:01:32,880
en nu zijn we min of meer stabiel rond 12.000.

20
00:01:32,950 --> 00:01:36,600
Het is heel onvoorspelbaar, niemand weet wanneer het gaat ontploffen.

21
00:01:36,680 --> 00:01:37,770
-Wanneer is het begonnen?

22
00:01:37,860 --> 00:01:43,400
De instorting begon rond 2018

23
00:01:43,510 --> 00:01:54,640
maar de inflatie op de markt is vanaf midden 2019, begin 2020 begonnen, denk ik.

24
00:01:55,950 --> 00:02:01,260
-Wanneer dat gebeurt, welke invloed heeft dat op het leven van mensen?

25
00:02:02,600 --> 00:02:07,020
Het is beangstigend, want de salarissen in Libanese ponden zijn niet gestegen.

26
00:02:07,060 --> 00:02:12,200
Het minimumloon was ongeveer 400 €.

27
00:02:12,200 --> 00:02:17,350
Nu is het nog maar 40 €,

28
00:02:17,880 --> 00:02:21,620
maar alles wat je consumeert is duurder

29
00:02:21,680 --> 00:02:24,750
omdat het ook geïndexeerd is aan de dollar, het wordt verschrikkelijk.

30
00:02:24,840 --> 00:02:27,910
Stel je voor dat je een karton melk koopt.

31
00:02:27,930 --> 00:02:31,530
Een karton melk dat 3000 Libanese ponden kostte

32
00:02:31,530 --> 00:02:36,480
kost nu 27 of 25.000,

33
00:02:36,600 --> 00:02:40,200
terwijl je nog steeds 100.000 verdient, bijvoorbeeld.

34
00:02:40,200 --> 00:02:44,160
Dus in plaats van elke maand enkele kartons te kunnen kopen

35
00:02:44,260 --> 00:02:47,380
kan je er nauwelijks één betalen.

36
00:02:47,440 --> 00:02:59,180
Aangezien onze consumptie voor 90-95% afhankelijk is van invoer,

37
00:02:59,260 --> 00:03:01,580
wanneer die afhankelijk is van een vreemde munt

38
00:03:01,620 --> 00:03:03,260
-de dollar in ons geval-,

39
00:03:03,310 --> 00:03:05,520
wordt alles veel duurder:

40
00:03:05,570 --> 00:03:08,210
brood wordt duurder,

41
00:03:08,360 --> 00:03:11,450
brandstof wordt duurder

42
00:03:11,560 --> 00:03:14,660
en de lonen zijn nog steeds dezelfde.

43
00:03:14,690 --> 00:03:17,720
Dat betekent de levensstandaard,

44
00:03:17,750 --> 00:03:20,820
de kwaliteit van het leven een zware klap krijgt

45
00:03:20,910 --> 00:03:23,090
en toegang tot voedsel

46
00:03:23,110 --> 00:03:26,530
en voedselzekerheid krijgen heel zware klappen.

47
00:03:26,720 --> 00:03:30,370
Daarom is het zo belangrijk om je eigen zaden te produceren

48
00:03:30,430 --> 00:03:33,200
in plaats van ze te importeren of elders te kopen

49
00:03:33,220 --> 00:03:35,490
vooral als ze hybride en steriel zijn

50
00:03:35,520 --> 00:03:38,230
en je er volgend jaar opnieuw moet kopen.

51
00:03:40,010 --> 00:03:43,980
Ook andere landbouwinputs hebben we dringend nodig

52
00:03:44,020 --> 00:03:46,480
biologische natuurlijk,

53
00:03:46,530 --> 00:03:49,810
zelf meststoffen of compost maken...

54
00:03:49,890 --> 00:03:51,540
het hele pakket, zoals dat heet.

55
00:03:51,570 --> 00:03:55,290
Dat proberen we te doen door trainingen

56
00:03:55,330 --> 00:03:57,330
en door het werk op de boerderij.

57
00:03:58,730 --> 00:04:01,460
-Toen je in Frankrijk was toonde je ons

58
00:04:01,470 --> 00:04:04,110
foto's van hoe,

59
00:04:04,160 --> 00:04:07,630
na wat jij de revolutie noemde,

60
00:04:07,640 --> 00:04:10,560
het leven voor heel veel mensen veranderde,

61
00:04:10,660 --> 00:04:12,610
allerlei mensen:

62
00:04:12,630 --> 00:04:18,410
studenten, ambtenaren,

63
00:04:18,430 --> 00:04:20,730
mensen uit alle lagen van de bevolking die komen

64
00:04:20,750 --> 00:04:23,360
en hier opeens interesse voor hebben.

65
00:04:23,630 --> 00:04:24,810
Precies.

66
00:04:24,930 --> 00:04:27,530
Het is alsof we al aanvoelden

67
00:04:28,180 --> 00:04:33,380
dat we toen al in een nooit geziene economische crisis zaten.

68
00:04:33,490 --> 00:04:38,210
Dat is waarom de revolutie begonnen is.

69
00:04:38,270 --> 00:04:43,040
Dus mensen waren zich vaag bewust

70
00:04:43,040 --> 00:04:47,650
dat er moeilijke jaren zouden komen

71
00:04:47,680 --> 00:04:50,490
en dat ze zouden moeten doorbijten.

72
00:04:50,540 --> 00:04:54,340
We zijn meer en meer bewust

73
00:04:54,400 --> 00:05:00,000
dat een economie die niet productief is, niet duurzaam is.

74
00:05:00,080 --> 00:05:03,130
En onze economie was niet productief.

75
00:05:03,460 --> 00:05:05,920
Heel lang, 30, 40 jaar,

76
00:05:05,970 --> 00:05:10,250
was onze economie op schulden gebaseerd.

77
00:05:11,090 --> 00:05:15,320
-Dus zijn veel mensen beginnen produceren?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
Dit wordt opgenomen.

80
00:05:20,430 --> 00:05:23,260
Sorry, ik zei iets aan mijn collega.

81
00:05:23,280 --> 00:05:26,070
-Zijn jullie met velen op de boerderij?

82
00:05:26,850 --> 00:05:31,620
Er zijn 16 volwassenen en 27 kinderen

83
00:05:31,650 --> 00:05:34,160
en momenteel hebben we heel wat vrienden op bezoek,

84
00:05:34,210 --> 00:05:36,000
vrijwilligers, stagiaires, ...

85
00:05:36,030 --> 00:05:38,930
In totaal zijn we nu met ongeveer 50,

86
00:05:39,000 --> 00:05:40,370
misschien zelfs meer.

87
00:05:40,410 --> 00:05:43,350
-En hoe groot is jullie plek ginder?

88
00:05:44,940 --> 00:05:47,770
Op de boerderij met de vereniging

89
00:05:47,920 --> 00:05:54,140
we hebben twee hectare land:

90
00:05:54,580 --> 00:05:57,940
18.000 m² beplante oppervlakte

91
00:05:57,950 --> 00:06:00,500
en 2000 m² tussen de gebouwen,

92
00:06:00,670 --> 00:06:02,980
kinderdagverblijven,

93
00:06:03,070 --> 00:06:06,050
de bassecourt voor kippen,

94
00:06:06,060 --> 00:06:07,640
geiten en schapen.

95
00:06:07,710 --> 00:06:09,820
En dit jaar waren we in staat om

96
00:06:09,830 --> 00:06:13,490
een extra 70.000 vierkante meter te huren,

97
00:06:13,610 --> 00:06:16,600
7 hectare meer denk ik,

98
00:06:16,700 --> 00:06:19,700
om onze collectie te vermeerderen en uit te breiden,

99
00:06:19,720 --> 00:06:21,350
vooral granen.

100
00:06:22,280 --> 00:06:24,770
-Maar je woont niet op de boerderij?

101
00:06:25,610 --> 00:06:27,510
Niet echt.

102
00:06:27,520 --> 00:06:32,860
Een van onze collega's en medeoprichter woont op de boerderij.

103
00:06:34,400 --> 00:06:36,320
We hebben een soort gebouw

104
00:06:36,340 --> 00:06:39,780
dat is een verwerkingskeuken,

105
00:06:39,820 --> 00:06:43,090
keuken voor ons, voorraadkamer, school

106
00:06:43,140 --> 00:06:44,350
en plat,

107
00:06:44,360 --> 00:06:47,040
een nogal lang gebouw.

108
00:06:47,060 --> 00:06:50,120
Het is Walid die daar woont met zijn familie.

109
00:06:50,260 --> 00:06:52,440
Na onze andere collega's en vrienden

110
00:06:52,460 --> 00:06:54,260
wonen recht tegenover de boerderij,

111
00:06:54,310 --> 00:06:56,530
en we zijn een beetje verder op in de straat,

112
00:06:56,550 --> 00:06:58,450
4 minuten te voet.

113
00:06:59,590 --> 00:07:02,290
-En je produceert ook een beetje

114
00:07:02,300 --> 00:07:04,400
wat je zelf consumeert?

115
00:07:04,740 --> 00:07:06,950
Ja !

116
00:07:07,160 --> 00:07:10,070
We kopen heel weinig.

117
00:07:10,110 --> 00:07:13,770
Alles wat we kunnen produceren, kopen we niet, dat hebben we!

118
00:07:14,900 --> 00:07:17,980
Er zijn dingen zoals rijst,

119
00:07:18,010 --> 00:07:20,880
het is een beetje ingewikkeld voor ons om te doen,

120
00:07:20,910 --> 00:07:22,690
dat soort dingen.

121
00:07:22,920 --> 00:07:24,120
Maar voor de rest…

122
00:07:24,130 --> 00:07:27,230
Vorig jaar zijn we er zelfs in geslaagd om wat honing te krijgen,

123
00:07:27,250 --> 00:07:30,520
maar er is een roofdier dat onze bijen opeet,

124
00:07:30,560 --> 00:07:32,600
dus we hebben nog maar 3 korven,

125
00:07:32,620 --> 00:07:34,570
terwijl wij er 11 hadden.

126
00:07:36,830 --> 00:07:39,980
Het idee is ook om te laten zien dat het mogelijk is.

127
00:07:41,590 --> 00:07:46,920
-Op het niveau van partners, schakels, netwerk

128
00:07:46,940 --> 00:07:50,600
die je hebt in Libanon en ook in Syrië,

129
00:07:50,610 --> 00:07:54,570
hoe het zich ontwikkelt, waar het vandaan komt?

130
00:07:54,590 --> 00:07:56,450
Vertel ons er over!

131
00:07:57,100 --> 00:08:00,160
In principe was het idee om eerst zaad te produceren

132
00:08:00,180 --> 00:08:03,170
en dan diegenen te onderwijzen die geïnteresseerd zijn

133
00:08:03,190 --> 00:08:04,560
om ze zelf te produceren,

134
00:08:04,590 --> 00:08:08,450
zodat je het niet elke keer hoeft te doen.

135
00:08:08,450 --> 00:08:11,030
Het is echt leren om onafhankelijk te zijn

136
00:08:11,040 --> 00:08:12,560
en onafhankelijkheid.

137
00:08:12,580 --> 00:08:16,380
Dus, vanaf dat punt, kleine producenten

138
00:08:16,400 --> 00:08:17,910
verspreid over de hele plaats.

139
00:08:17,920 --> 00:08:19,710
Er zijn er die teruggekeerd zijn naar Syrië

140
00:08:19,710 --> 00:08:22,260
om hun zaden zelf te produceren in Syrië.

141
00:08:22,280 --> 00:08:25,120
Er is echt een beetje van alles.

142
00:08:25,150 --> 00:08:29,610
Onze vrienden die we al sinds het begin volgen

143
00:08:29,630 --> 00:08:32,020
zijn nu volledig onafhankelijk,

144
00:08:32,040 --> 00:08:34,970
Ze doen het heel goed voor zichzelf.

145
00:08:34,980 --> 00:08:36,910
Natuurlijk volgen we het op,

146
00:08:36,920 --> 00:08:40,350
als er behoefte is aan uitwisseling van informatie, van ervaring:

147
00:08:40,360 --> 00:08:42,840
"Wij hebben dit getest", "zij hebben dit getest".

148
00:08:42,920 --> 00:08:46,690
Het idee is om zoveel mogelijk producenten te hebben,

149
00:08:46,690 --> 00:08:49,360
zoveel mogelijk zaadhokken

150
00:08:49,390 --> 00:08:51,640
in heel Libanon

151
00:08:51,690 --> 00:08:55,600
en misschien zelfs Syrisch op een dag, we weten het niet.

152
00:08:57,460 --> 00:09:02,240
-Het is niet duidelijk, de grens is nog steeds gesloten?

153
00:09:03,970 --> 00:09:06,550
Ja, het is gesloten.

154
00:09:06,570 --> 00:09:09,540
en het is nogal ingewikkeld voor mensen

155
00:09:09,610 --> 00:09:12,040
van bepaalde nationaliteiten om er ook door te komen.

156
00:09:12,050 --> 00:09:16,250
Als je geen deel uitmaakt van een diplomatieke missie of het leger

157
00:09:16,400 --> 00:09:17,990
het is nog steeds moeilijk -

158
00:09:18,660 --> 00:09:20,860
is het beter om het niet te doen.

159
00:09:21,700 --> 00:09:23,550
Je kunt beter blijven waar je bent.

160
00:09:23,580 --> 00:09:25,730
-In termen van opleiding.

161
00:09:25,920 --> 00:09:30,670
Je hebt meegewerkt aan de vertaling van DIYseeds -

162
00:09:34,330 --> 00:09:37,440
Heb je de site vertaald?

163
00:09:38,750 --> 00:09:40,100
... de site in het Arabisch,

164
00:09:40,130 --> 00:09:44,680
en ik heb de video's bekeken die door een ander team zijn gemaakt

165
00:09:44,820 --> 00:09:47,750
wat toch een hoop werk is,

166
00:09:47,760 --> 00:09:50,090
het is verbazend wat ze gedaan hebben, het is zo mooi.

167
00:09:50,170 --> 00:09:52,080
Het is zo handig.

168
00:09:52,120 --> 00:09:54,720
Als je een training geeft

169
00:09:54,780 --> 00:09:57,840
waar je probeert uit te leggen hoe je

170
00:09:57,870 --> 00:10:01,660
of hoe je veilige afstanden maakt

171
00:10:01,670 --> 00:10:03,360
om kruisingen te vermijden,

172
00:10:03,380 --> 00:10:05,360
het is beter met de illustraties,

173
00:10:05,360 --> 00:10:08,140
de tekeningen, de video en de uitleg.

174
00:10:08,140 --> 00:10:11,550
Niet alleen laten we deze video's zien tijdens de trainingen,

175
00:10:11,560 --> 00:10:13,470
maar omdat ze toegankelijk zijn

176
00:10:13,480 --> 00:10:17,000
naar mensen direct op hun telefoon

177
00:10:17,020 --> 00:10:19,660
-wanneer ze internet hebben, maar hey, het is toegankelijk-,

178
00:10:19,680 --> 00:10:23,800
het stelt mensen in staat heel goed alleen te functioneren.

179
00:10:23,910 --> 00:10:25,680
Het is perfect, het is perfect.

180
00:10:27,600 --> 00:10:30,770
-En het is ook makkelijk voor mensen in de buurt

181
00:10:31,240 --> 00:10:35,560
om deze informatie te hebben, om het te gebruiken, om…

182
00:10:35,640 --> 00:10:36,480
Dat is juist!

183
00:10:36,540 --> 00:10:37,730
Het maakt het makkelijker voor hen,

184
00:10:37,760 --> 00:10:40,500
omdat het een vrij licht, simpel ding is.

185
00:10:40,520 --> 00:10:44,910
Het is geen groot boek dat je mee moet nemen.

186
00:10:44,960 --> 00:10:47,940
Er zijn veel boeren die niet kunnen lezen,

187
00:10:48,000 --> 00:10:50,220
maar ze kunnen horen.

188
00:10:50,260 --> 00:10:52,970
Dan zijn er nog de tekeningen en de video zelf,

189
00:10:53,040 --> 00:10:55,570
dus het is veel meer illustratief

190
00:10:55,580 --> 00:10:58,770
en veel toegankelijker en het is eenvoudiger,

191
00:10:58,800 --> 00:11:01,830
als een handleiding is het makkelijker om dit toe te passen

192
00:11:01,870 --> 00:11:05,270
dan je grote boek te pakken en te ontcijferen.

193
00:11:06,000 --> 00:11:11,350
-Zoals jij niet-hybride zaden produceert,

194
00:11:11,380 --> 00:11:14,580
natuurlijk, reproduceerbaar zaad,

195
00:11:14,660 --> 00:11:18,030
staat het ook iedereen toe die deze zaden heeft

196
00:11:18,160 --> 00:11:21,380
en toegang tot de video's om meteen zelfstandigheid te krijgen.

197
00:11:21,400 --> 00:11:26,560
Kun je ons vertellen hoe, hoe snel het kan werken?

198
00:11:26,580 --> 00:11:27,630
Welke snelheid?

199
00:11:27,650 --> 00:11:30,530
Het hangt af van de toegang tot het land,

200
00:11:30,560 --> 00:11:32,160
Het hangt af van de toegang tot water,

201
00:11:32,180 --> 00:11:34,310
het hangt af van de veiligheid van het gebied.

202
00:11:34,320 --> 00:11:37,320
We hebben vrienden die verschillende keren moesten verhuizen

203
00:11:37,390 --> 00:11:39,260
uit hun gebied in Syrië.

204
00:11:39,300 --> 00:11:41,170
Elke keer als ze binnenkwamen

205
00:11:41,180 --> 00:11:42,830
waren er bomaanslagen,

206
00:11:42,840 --> 00:11:44,810
dus moesten ze opnieuw opstarten en zo verder.

207
00:11:44,960 --> 00:11:47,240
De context zelf is ingewikkeld.

208
00:11:47,320 --> 00:11:51,670
Maar als je het basisgereedschap hebt,

209
00:11:51,710 --> 00:11:54,660
waar je ook vandaan komt, je kunt het nog steeds aan.

210
00:11:54,690 --> 00:11:57,110
Je hebt je zaadjes, je weet hoe je ze moet planten,

211
00:11:57,130 --> 00:12:00,260
je weet hoe ze te reproduceren, haal ze eruit,

212
00:12:00,290 --> 00:12:01,990
bewaar ze, bewaar ze, etc.

213
00:12:02,020 --> 00:12:04,980
Je bent een vrije man of vrouw!

214
00:12:07,140 --> 00:12:08,050
Dat is het.

215
00:12:08,190 --> 00:12:12,120
-Dus je ziet mensen die, in één seizoen,

216
00:12:12,170 --> 00:12:15,180
geleerd om weer zaden te maken

217
00:12:15,580 --> 00:12:19,470
en nu hebben ze ook hun eigen zaden?

218
00:12:19,520 --> 00:12:20,510
Precies.

219
00:12:20,530 --> 00:12:23,240
De meeste van de mensen waar we mee werken

220
00:12:23,290 --> 00:12:25,350
hebben een agrarische achtergrond,

221
00:12:25,370 --> 00:12:26,840
of een landbouwachtergrond

222
00:12:26,850 --> 00:12:29,000
of die in de landbouw hebben gewerkt,

223
00:12:29,020 --> 00:12:30,440
zelfs als het conventioneel is.

224
00:12:30,460 --> 00:12:34,350
Het is makkelijker voor hen om die overgang te maken.

225
00:12:36,550 --> 00:12:38,500
In principe moesten ze

226
00:12:38,520 --> 00:12:39,970
om zaden te kopen

227
00:12:39,970 --> 00:12:42,450
en de tweede leerden ze

228
00:12:42,470 --> 00:12:45,010
dat hun lot in hun handen lag,

229
00:12:45,260 --> 00:12:47,100
omdat ze gewoon ontbraken

230
00:12:47,120 --> 00:12:50,730
het juiste gereedschap om dat te doen,

231
00:12:50,880 --> 00:12:53,890
in een seizoen of twee hebben ze hun zaad,

232
00:12:53,930 --> 00:12:55,040
we ruilen.

233
00:12:55,080 --> 00:12:56,800
We zitten er voor de lange termijn in,

234
00:12:56,820 --> 00:13:00,570
bijvoorbeeld iemand produceert een variëteit,

235
00:13:00,580 --> 00:13:03,410
iemand produceert een ander, wij doen de derde…

236
00:13:03,423 --> 00:13:08,680
gewoon om zoveel mogelijk diversiteit te hebben en dat is het,

237
00:13:08,720 --> 00:13:10,680
het begint te werken.

238
00:13:11,760 --> 00:13:16,280
-En jij, hoe raakte jij betrokken in dit alles?

239
00:13:18,426 --> 00:13:20,280
Echt toevallig.

240
00:13:20,330 --> 00:13:26,600
Ik woonde in Beiroet en mijn kamergenoot zat op de universiteit met Ferdi,

241
00:13:26,693 --> 00:13:30,613
die een van de medeoprichters van de vereniging en het project is.

242
00:13:32,320 --> 00:13:35,253
Dus toen Ferdi en Lara terugkeerden naar Libanon

243
00:13:35,293 --> 00:13:38,253
om te zien hoe je het zaad project start,

244
00:13:38,290 --> 00:13:41,270
hoe te produceren, waar dit alles te doen,

245
00:13:41,308 --> 00:13:43,228
wat er moet komen,

246
00:13:43,242 --> 00:13:44,927
ze bleven bij ons

247
00:13:44,936 --> 00:13:46,592
voor ongeveer een maand

248
00:13:46,602 --> 00:13:48,545
en het klikte meteen en dat was het.

249
00:13:49,727 --> 00:13:51,303
Zoals ik al zei:

250
00:13:51,336 --> 00:13:54,174
Ik heb me bewust laten inpalmen!

251
00:13:54,988 --> 00:13:56,898
-En je hebt je bekeerd?

252
00:13:57,068 --> 00:13:59,101
Wat deed je daarvoor?

253
00:14:00,112 --> 00:14:03,303
Direct voordat ik communicatie deed

254
00:14:03,336 --> 00:14:04,823
voor een universiteit.

255
00:14:04,851 --> 00:14:06,960
Maar ik ben een boekverkoper in hart en nieren,

256
00:14:09,520 --> 00:14:12,640
Ik studeerde psychologie, literatuur,

257
00:14:12,672 --> 00:14:15,868
niets te maken met zaden!

258
00:14:19,745 --> 00:14:21,520
Maar het helpt om te vertalen,

259
00:14:21,543 --> 00:14:22,922
dus het is perfect!

260
00:14:23,275 --> 00:14:24,992
-En je wilt doorgaan?

261
00:14:25,920 --> 00:14:27,637
Ja!

262
00:14:27,661 --> 00:14:29,863
Zoals we al eerder zeiden:

263
00:14:29,890 --> 00:14:33,002
we zitten midden in een noodsituatie,

264
00:14:34,164 --> 00:14:39,185
Ik denk dat het echt essentieel werk is,

265
00:14:40,014 --> 00:14:41,477
het is nodig.

266
00:14:41,515 --> 00:14:47,298
En het is verrijkend op vele niveaus:

267
00:14:47,310 --> 00:14:50,555
op sociaal niveau, ook op politiek niveau.

268
00:14:50,597 --> 00:14:53,050
Het is een uiterst belangrijke activiteit

269
00:14:53,072 --> 00:14:54,823
en mooi op hetzelfde moment.

270
00:14:55,247 --> 00:14:58,211
Ik denk niet dat ik snel zal stoppen!

271
00:14:58,418 --> 00:15:01,797
-Geweldig. Bedankt voor het interview.

272
00:15:01,960 --> 00:15:03,722
Graag gedaan, graag gedaan!

273
00:15:03,755 --> 00:15:06,290
Het was ook leuk om met jou te praten.

274
00:15:07,620 --> 00:15:08,936
-Ik hoop dat we elkaar nog eens zien.

275
00:15:08,960 --> 00:15:11,134
We wachten op je! We wachten op je op de boerderij!

276
00:15:11,181 --> 00:15:15,811
-Ik wil wel:) Ik kan niets beloven, maar…

277
00:15:17,463 --> 00:15:20,560
Het is wanneer je maar wilt, je vertelt ons dat je komt

278
00:15:20,583 --> 00:15:22,672
en "ahla of sahla" (welkom!)

279
00:15:25,251 --> 00:15:26,409
-Hartelijk dank.

280
00:15:26,432 --> 00:15:27,091
Dank je wel.
