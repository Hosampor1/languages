# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: fennel\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"
"Language: fr\n"

msgid "Fennel belongs to the Apiaceae family and the Foeniculum vulgare species. It can be annual, biennial and under certain conditions perennial. There are two different types:"
msgstr "Le fenouil de la famille des Apiaceae et de l'espèce Foeniculum vulgare est une plante annuelle, bisannuelle et parfois vivace dans certaines conditions. On trouve deux types différents :"

msgid "Fennel with a bulb called Florence fennel"
msgstr "le fenouil à bulbe appelé fenouil de Florence"

msgid "and herb fennel without a bulb, cultivated for its leaves and seeds, used for medicinal or aromatic reasons."
msgstr "et le fenouil sans bulbe, cultivé pour ses feuilles et ses graines à usage médicinal et aromatique."


msgid "There are both early and late varieties. Fennel is a photoperiodical plant, which means that long days encourage flowering."
msgstr "Certaines variétés sont de culture précoce, d'autres tardive. Le fenouil est une plante photopériodique, ce qui veut dire que les jours longs favorisent la montée en fleur."

msgid "The inflorescence of fennel is an umbel composed of small yellow flowers that are usually hermaphrodite. The stamen, the male sexual organ, matures before the pistil, the female organ."
msgstr "L'inflorescence du fenouil est une ombelle composée de petites fleurs jaunes généralement hermaphrodites. Or les étamines, organe sexuel mâle sont mûres avant le pistil, l'organe femelle."

msgid "Self-fertilization does not therefore occur within the same flower. Yet since the flowers do not bloom at the same time, self-fertilization is possible within the same umbel or between two umbels on the same plant. Fertilization also occurs between the umbels of different plants."
msgstr "Il n'y a donc pas d'autofécondation au sein de la même fleur. Mais comme les fleurs ne s'épanouissent pas en même temps, il peut y avoir autofécondation au sein de la même ombelle ou entre deux ombelles de la même plante. Il y aura aussi fécondation entre des ombelles de plantes différentes."

msgid "Fennel is therefore an allogamous plant that is mainly pollinated by insects. There is a risk of cross-pollination between different varieties."
msgstr "Le fenouil est donc une plante allogame et ce sont les insectes qui constituent le principal facteur de pollinisation. Il existe un risque de pollinisation croisée entre différentes variétés."

msgid "Fennel can also cross with wild fennel, which is very common in certain regions of the world."
msgstr "Le fenouil peut se croiser également avec le fenouil sauvage très présent dans certaines régions du monde."

msgid "To avoid cross-pollination, two varieties of fennel should be grown one kilometer apart. This distance can be reduced to 500 meters if there's a natural barrier, such as a hedge."
msgstr "Afin d'éviter les croisements inter-variétaux, 2 variétés de fenouil seront cultivées à une distance de 1 kilomètre l'une de l'autre. Cette distance peut être réduite à 500 mètres s'il existe une barrière naturelle comme une haie."

msgid "The varieties can also be isolated by alternately opening and closing mosquito nets or by placing small hives with insects inside a closed mosquito net (for this technique, see the module on isolation techniques in the “ABC of seed production”)."
msgstr "Pour cultiver deux variétés côte à côte dans le même jardin, on peut pratiquer l'isolement variétal avec des moustiquaires ouvertes en alternance ou avec des ruchettes d'insectes sous moustiquaire fixe. On se reportera au module sur les techniques d'isolement dans « l'ABC de la production de semences »."

msgid "Herb fennel does not form a bulb. It is perennial in most regions. It is sown in spring, it flowers and produces seeds in the first year. It resists well to frosts and can stay in the ground over winter. The amount of seeds produced will be greater in the second year of cultivation."
msgstr "Le fenouil aromatique ne forme pas de bulbe. Il est vivace dans la plupart des régions. Il est semé au printemps, monte en fleur et produit des graines la première année. Il résiste bien au gel et pourra rester en place l'hiver. La récolte des graines sera plus abondante à partir de la deuxième année de culture."

msgid "Bulb fennel can be grown in different ways."
msgstr "Le fenouil à bulbe, quant à lui, peut suivre différents modes de culture."

msgid "In a mild climate spring varieties are grown as annual plants. They are sown early in spring, in March, which gives them time to form a bulb before they flower."
msgstr "En climat doux, on cultive les variétés de printemps en plantes annuelles. On les sème tôt au printemps, en mars, ce qui leur donne le temps de se former avant de monter en fleur."

msgid "In most cases, bulb fennel grown for seed is cultivated as a biennial. Summer varieties are sown after 20 June when the days start getting shorter. This will prevent them from flowering too early. They will have the time to form their bulbs before winter. They will flower and produce seeds in the second year."
msgstr "Mais dans la plupart des cas, le fenouil porte-graines se cultive comme une plante bisanuelle. On sème les variétés d'été après le 20 juin lorsque les jours déclinent, pour éviter une montée en fleur trop précoce. Ils auront le temps de faire leur bulbe avant l'hiver. Ils monteront en fleurs et produiront leurs graines la deuxième année."

msgid "Only plants with the typical characteristics of the variety that have formed bulbs are chosen for seed."
msgstr "On veillera à garder comme porte-graines uniquement ceux qui auront formé leur bulbes."

msgid "Plants that flower too early are removed. Select the bulbs for seed production according to the specific characteristics of the variety: colour, shape, vigour."
msgstr "On écartera ceux montant en fleur trop précocément. On sélectionnera les porte-graines en fonction des caractéristiques propres de la variété : la couleur, la forme, la vigueur."

msgid "In a cold climate, the bulbs may freeze in the ground. This is why you should remove the leaves above the last bud and put the bulbs in a jar or pot with their roots and soil, protected from the frost and too much humidity."
msgstr "Sous un climat froid, les bulbes pourraient geler en terre. C'est pourquoi, après avoir écimé le feuillage au dessus du bourgeon terminal, on les conservera en jauge ou en pot avec leurs racines et leur motte, à l'abri du gel et sans trop d'humidité."

msgid "You should check the bulbs throughout the winter and eliminate any that have started to rot. They will be replanted in spring when the risk of a hard frost has passed."
msgstr "Tout au long de l'hiver, on inspecte les bulbes afin d'éliminer ceux qui pourrissent. Ils seront remis en terre au printemps quand les risques de grosses gelées seront passés."

msgid "Fifteen to twenty plants are required to ensure good genetic diversity."
msgstr "Quinze à vingt porte-graines sont nécessaires pour maintenir une bonne diversité génétique."

msgid "In mild regions, bulb fennel can be left in the ground over winter. It will continue to grow in the spring of its second year and then flower."
msgstr "Dans les régions clémentes, on peut laisser le fenouil bulbe en place pendant l'hiver. Il renouvelle sa végétation avec le retour du printemps puis il montera en fleur."

msgid "Fennel plants have very high floral stalks that tend to fall over. They should be staked."
msgstr "Les fenouils portent des hampes florales très hautes qui ont souvent tendance à verser. Il faudra donc les tuteurer."

msgid "The umbels are cut with a piece of the stalk when the first mature seeds start to fall. They can be cut earlier; the seeds will continue to mature slowly during the drying process."
msgstr "Les ombelles sont coupées avec un morceau de tige quand les premières semences mûres commencent à tomber. On peut cependant commencer plus tôt, le mûrissement des graines s'opérant doucement pendant le séchage."

msgid "In any case, they should continue to be dried in a dry and well ventilated place."
msgstr "Dans tous les cas il est conseillé de continuer le séchage dans un endroit sec et ventilé."

msgid "Rub the umbels between your hands to remove the seeds."
msgstr "L'extraction s'effectue en frottant les ombelles avec les mains."

msgid "To sort the seeds use first of all coarse mesh sieves that retain the chaff. Then the seeds are retained by a finer sieve that allows dust to pass through."
msgstr "Pour le tri, on utilise d'abord des tamis grossiers qui retiennent les déchets. Puis un tamis fin qui retiendra les graines et évacuera les poussières."

msgid "Finally winnow the seeds by blowing on them. This will get rid of any remaining chaff."
msgstr "Enfin il faudra les vanner c'est-à-dire souffler dessus pour faire partir les derniers déchets."

msgid "Always include a label with the name of the variety and species as well as the year in the bag as writing on the bag can be rubbed off."
msgstr "On doit toujours mettre une étiquette avec le nom de la variété et de l'espèce ainsi que l'année de production dans le sachet car il arrive parfois que l'inscription sur le sachet s'efface."

msgid "Storing the seeds in the freezer for several days kills certain parasite larvae."
msgstr "Un séjour au congélateur de quelques jours éliminera certaines larves de parasites."

msgid "Fennel seeds have a germination capacity of four years. Sometimes this can be extended to seven years. This will be further prolonged by storing the seeds in the freezer. One gram contains around 300 seeds."
msgstr "Les semences de fenouil ont une faculté germinative de quatre ans qui peut parfois se prolonger jusqu'à 7 ans. Cette faculté est prolongée par un stockage à basse température. Un gramme de semences contient environ 300 graines."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your fennel seeds: video explanations"
msgstr "Produire ses graines de fenouil : les explications en vidéo"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own fennel seeds: pollination, life cycle, extraction, sorting and storing."
msgstr "Cette vidéo vous expliquera, pas à pas, comment produire vos propres semences de fenouil : pollinisation, cycle de vie, extraction, tri et conservation."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, fennel, step by step, explanations"
msgstr "apprendre, comment faire, produire, semences, vidéo, fenouil, pas à pas, explications"
