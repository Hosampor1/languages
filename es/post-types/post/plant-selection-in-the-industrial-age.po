# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"WPOT-Type: post\n"
"WPOT-Origin: plant-selection-in-the-industrial-age\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Plant selection in the industrial age"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "plant-selection-in-the-industrial-age"
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Plant selection in the industrial age"
msgstr ""

msgid "Living creatures reproduce themselves for free, while industry’s raison d’être is profit."
msgstr ""

msgid "The Law of Life is opposed to the Law of Profit."
msgstr ""

msgid "Life exists thanks to the singularity of each organism, industry asserts itself through the uniformity of its goods."
msgstr ""

msgid "For industrial capitalism life is a two-fold sacrilege."
msgstr ""

msgid "Over the last two centuries, vanquishing this double sacrilege has been the historic task that industrial capitalism has entrusted plant to plant breeders and agronomic sciences. This task is now almost completed. You only have to see the oversize, over-green industrial fields scored by the scars left by the wheels of tractors, in which no plant is higher than its neighbours, to realize that uniformity has triumphed. As to the other sacrilege, the patenting of life crowns two centuries of efforts aimed at putting an end to the founding practice of agriculture, that of resowing one’s own harvested seed. This means separating production from reproduction, and making reproduction the privilege of a few. Today this privilege belongs to the cartel of “life sciences” – the producers of pesticides, herbicides, insecticides, larvicides, ovocides, gametocides, bactericides, molluscicides, rodenticides, acaricides, fungicides : the “Cartel of Cides”!"
msgstr ""

msgid "Industrial selection is simple if you break through the genetic smokescreen. It consists of replacing a variety, the characteristic of which is being varied and therefore the opposite of uniformity, with copies of a selected plant within the variety – with clones. This signaled the death-bell for the sacrilege of diversity. There remained, however, the other sacrilege to be dealt with, which meant putting an end to reproduction free of charge. Already in its early days the new science of genetics achieved this miracle. The result was “hybrid” corn, the sacred cow of agronomic sciences and “paradigm” of plant breeding of the 20th century."
msgstr ""

msgid "In 1836, John Le Couteur, an English gentleman farmer - a capitalist who invested his capital in agricultural production - codified the technique of “isolation” practiced empirically since the turn of the century. Since we cultivate, he argues, varieties (the character of that which is varied, diversity) of plants, since each plant retains its individual characters, I will “isolate” in my fields the most promising plants to cultivate them. individually [1] and therefore reproduce and multiply them - to make copies, clones - to finally select the best clone and replace the variety. The technique of isolation is based on an unstoppable logical principle. There is always a gain in replacing a variety of “anything” with copies of a better “anything” isolated within the variety."
msgstr ""

msgid "It remains to put an end to the sacrilege of gratuity. An autogamous clone reproduces identically. The harvested grain is also the seed of the following year. It was not until the 1920s for this problem to find the beginnings of an administrative / legal solution in France and the 21st century for the patent to put an end to the scandal of free reproduction."
msgstr ""

msgid "At the end of the 1920s French technocratic agronomists imposed a new system according to which varieties put on to the market must be “homogenous” and “stable”. Homogenous means that the plants must be phenotypically (visually) identical, while to be considered stable the same plant must be put on the market every year. This dual requirement means that the plants must be genetically identical or almost so. Their homogeneity and stability are controlled by an official body. If a new variety satisfies these criteria, it is registered in a Catalogue and the approved breeder receives a certificate which gives him the exclusive right to put the variety on to the market. In 1960 this mechanism was adopted by the International Union for the Protection of New Varieties of Plants (UPOV) which has now been ratified by about sixty countries."
msgstr ""

msgid "The certificate protects the seed producer from “piracy” of his clones by competitors, but there were still too many farmers for the seed industry to dare to attack the “farmer’s privilege”. The certificate corresponded to the needs of traditional plant breeding companies run by agronomists passionate about their breeding work. Over the last thirty years, however, the “Cartel of Cides” has taken control over the world’s seeds. This cartel considers the farmer who sows the seeds he harvests to be a “pirate”. European directive 98/44 that authorizes the patenting of life is putting an end to this “piracy”."
msgstr ""

msgid "In 1900, the rediscovery of Mendel's laws extended the isolation method to corn. But in practice, it turns out to be so difficult, so improbable, that it was necessary to invent a biological phenomenon, always “unexplained and inexplicable [2]” - heterosis - to justify its implementation - then, as we we have seen, no justification is necessary! But “hybrid” corn puts an end to the double sacrilege and geneticists, breeders and breeders have been able to accredit and perpetuate the existence and their futile hunting of this genetic yeti."
msgstr ""

msgid "In summary, the history of industrial selection is that of mining by selection-cloning which destroys the diversity created by the friendly cooperation between peasants and nature since the beginnings of the domestication of plants and animals. Because the peasants did not wait for genetics to “improve” the plants. This is evidenced by the profusion of cultivated varieties (and animal breeds)."
msgstr ""

msgid "Genetics and breeding are separate activities. In the 1980s, I was fortunate enough to see a very great wheat breeder, Claude Benoît, at work in a disjunct wheat field. At first, for me, the plants were all the same. At the end of the day, I began to roughly distinguish them and understand Claude Benoît's selection criteria. “I was starting”, because Claude Benoît could not himself explain what made him retain this one rather than that one among the plants which seemed to me strictly similar. It is that selecting is based on an “uncoded” knowledge which cannot be made explicit or that it is with difficulty. The meticulous work of the breeder guided by experience, by a long familiarity with the plant, by empathy not to say the love that he and that she [3] has for him, by a sense acute observation, due to its agronomic knowledge, does not need the geneticist [4]. Rather, the esotericism of genetics serves to intimidate those who would like to make selection, to discourage them, to make them give up. As we have seen, the Geneticist, like other scientists, can make a mistake by deceiving us as long as he is not mistaken about the interests to be served."
msgstr ""

msgid "There is no point in shedding crocodile tears over the collapse of cultivated biodiversity when all the dynamics of industrial capitalism tend towards it and the regulatory and legislative system and repression framing the production and sale of seeds imposes a single two centuries old breeding method."
msgstr ""

msgid "While waiting for the struggles against the infamy of the living patent to be successful, for a legal framework imposing a selection method for the destruction of diversity, enacted to industrialize agriculture and eliminate peasants, to expel the Cartel des Cides de la Vie, organizing collectively to cultivate diversity, share seeds, disseminate the corresponding know-how, as generations of peasants have done before us, are acts of survival all over the world as much as resistance and freedom. Kokopelli has shown the way. Longo Maï describes, step by step, how to reclaim our seeds and our future."
msgstr ""

msgid "Jean-Pierre Berlan"
msgstr ""

msgid "former director of research, French National Institute of Agronomic Research (INRA)"
msgstr ""

msgid "[1] This individualization of the plant accompanies the rise of bourgeois individualism."
msgstr ""

msgid "[2] Terms used several times during the World Symposium (400 researchers!) Devoted to “Heterosis in crops” organized by the International Corn and Wheat Center in Mexico City."
msgstr ""

msgid "[3] An old INRA breeder told me this wonderful thing, a little embarrassed: “You know, when I'm alone with my plants, I talk to them”."
msgstr ""

msgid "[4] For this reason, the Cides cartel bought out the seed companies. Their molecular biologists, manipulators and manipulators of genes, are quite incapable of carrying out selection work."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "The history of plant selection in the industrial age"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This article summarizes the history of seed breeding or how the industry prevails through uniformity of commodities."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "selection, seeds, history, industry, uniformity, patents, varieties, capitalism"
msgstr ""
