# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-08-13 13:21+0000\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"
"WPOT-Type: post-types/person\n"
"WPOT-Origin: myleine-guiard-schmid\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Myleine Guiard Schmid"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "myleine-guiard-schmid"
msgstr ""

msgctxt "name"
msgid "Myleine Guiard Schmid"
msgstr ""

msgctxt "given-name"
msgid "Myleine"
msgstr ""

msgctxt "family-name"
msgid "Guiard Schmid"
msgstr ""

msgctxt "description"
msgid "Myleine Guiard-Schmid creates drawings and animated drawings for the cinema and also practices the visual arts. Cinema, sculpture, painting... The arts are her passion, her profession. Art has an undeniable power to reveal, to tell stories or denounce with beauty and style."
msgstr ""
