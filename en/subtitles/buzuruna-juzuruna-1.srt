﻿1
00:00:00,000 --> 00:00:05,780
(title)

2
00:00:05,920 --> 00:00:08,760
-What does "Buzuruna Juzuruna" mean?

3
00:00:09,520 --> 00:00:12,100
It means "our seeds, our roots".

4
00:00:12,140 --> 00:00:16,150
-And how does this name represent your association?

5
00:00:17,870 --> 00:00:25,170
We are a school-farm that produces peasant seeds

6
00:00:26,090 --> 00:00:30,570
and we also organize trainings.

7
00:00:30,660 --> 00:00:35,520
These are our two main activities.

8
00:00:36,220 --> 00:00:39,350
So we produce seeds

9
00:00:39,530 --> 00:00:42,460
which in turn become roots.

10
00:00:42,460 --> 00:00:47,020
The idea is to work towards peasant autonomy

11
00:00:47,130 --> 00:00:49,380
and food sovereignty.

12
00:00:49,380 --> 00:00:53,120
That all seems to sound well together.

13
00:00:54,080 --> 00:01:04,730
-Why is food autonomy so important in Lebanon right now?

14
00:01:05,600 --> 00:01:07,840
It is an emergency.

15
00:01:07,860 --> 00:01:13,600
We are in the middle of an unprecedented economic crisis,

16
00:01:13,600 --> 00:01:15,600
which is extremely violent.

17
00:01:15,710 --> 00:01:24,150
Our currency has lost 10 times its value.

18
00:01:24,260 --> 00:01:30,020
We went from 1500 Lebanese pounds for a dollar to 15,000 at one point

19
00:01:30,040 --> 00:01:32,880
and now we are more or less stabilized at 12,000.

20
00:01:32,950 --> 00:01:36,600
It's very random, we don't know when it's going to explode.

21
00:01:36,680 --> 00:01:37,770
-How long ago was this?

22
00:01:37,860 --> 00:01:43,400
The collapse started around 2018

23
00:01:43,510 --> 00:01:54,640
but we saw inflation in the market from mid 2019, early 2020 I think.

24
00:01:55,950 --> 00:02:01,260
-When that happens, how much of an impact does it have on people's lives?

25
00:02:02,600 --> 00:02:07,020
It's terrifying, because the salaries haven't increased in Lebanese pounds.

26
00:02:07,060 --> 00:02:12,200
The minimum wage was worth around 400 € a month.

27
00:02:12,200 --> 00:02:17,350
now it is only 40 €,

28
00:02:17,880 --> 00:02:21,620
but everything you consume is more expensive

29
00:02:21,680 --> 00:02:24,750
because it's also indexed to the dollar, it's becoming terrible.

30
00:02:24,840 --> 00:02:27,910
Imagine you're buying a carton of milk.

31
00:02:27,930 --> 00:02:31,530
A carton of milk that cost 3000 Lebanese pounds

32
00:02:31,530 --> 00:02:36,480
now costs 27 or 25,000,

33
00:02:36,600 --> 00:02:40,200
while you are still paid 100,000, for example.

34
00:02:40,200 --> 00:02:44,160
So instead of being able to buy several every month

35
00:02:44,260 --> 00:02:47,380
you can barely buy one.

36
00:02:47,440 --> 00:02:59,180
As our consumption depends on imports for 90-95%,

37
00:02:59,260 --> 00:03:01,580
when it depends on a foreign currency

38
00:03:01,620 --> 00:03:03,260
-the dollar in our case-,

39
00:03:03,310 --> 00:03:05,520
everything becomes much more expensive:

40
00:03:05,570 --> 00:03:08,210
bread becomes more expensive,

41
00:03:08,360 --> 00:03:11,450
fuel becomes more expensive

42
00:03:11,560 --> 00:03:14,660
and wages are still the same.

43
00:03:14,690 --> 00:03:17,720
That means the standard of living,

44
00:03:17,750 --> 00:03:20,820
the quality of life takes a big hit

45
00:03:20,910 --> 00:03:23,090
and access to food

46
00:03:23,110 --> 00:03:26,530
and food security take a very big hit.

47
00:03:26,720 --> 00:03:30,370
That's why it's so urgent to produce our own seeds

48
00:03:30,430 --> 00:03:33,200
instead of importing or buying them from elsewhere

49
00:03:33,220 --> 00:03:35,490
especially if they are hybrid and sterile

50
00:03:35,520 --> 00:03:38,230
and you have to buy more next year.

51
00:03:40,010 --> 00:03:43,980
We also need to produce our own fertilizers

52
00:03:44,020 --> 00:03:46,480
in an organic and clean way,

53
00:03:46,530 --> 00:03:49,810
make compost...

54
00:03:49,890 --> 00:03:51,540
The lot, as they say.

55
00:03:51,570 --> 00:03:55,290
This is what we try to do through trainings

56
00:03:55,330 --> 00:03:57,330
and through the activity of the farm.

57
00:03:58,730 --> 00:04:01,460
-When you were in France you showed us

58
00:04:01,470 --> 00:04:04,110
pictures of how,

59
00:04:04,160 --> 00:04:07,630
after what you called the revolution,

60
00:04:07,640 --> 00:04:10,560
life has changed for a lot of people,

61
00:04:10,660 --> 00:04:12,610
very diverse people:

62
00:04:12,630 --> 00:04:18,410
students, people who work in the administration,

63
00:04:18,430 --> 00:04:20,730
people from all walks of life who come

64
00:04:20,750 --> 00:04:23,360
and suddenly they're interested in that.

65
00:04:23,630 --> 00:04:24,810
Exactly.

66
00:04:24,930 --> 00:04:27,530
It's like we could feel it coming,

67
00:04:28,180 --> 00:04:33,380
that somewhere we were already in an unprecedented economic crisis.

68
00:04:33,490 --> 00:04:38,210
That's why the revolution started.

69
00:04:38,270 --> 00:04:43,040
So people were vaguely aware

70
00:04:43,040 --> 00:04:47,650
that the years to come were not going to be easy

71
00:04:47,680 --> 00:04:50,490
and that they'd have to manage somehow.

72
00:04:50,540 --> 00:04:54,340
We are more and more aware

73
00:04:54,400 --> 00:05:00,000
that an unproductive economy is not sustainable,

74
00:05:00,080 --> 00:05:03,130
and ours was not a productive economy.

75
00:05:03,460 --> 00:05:05,920
For a very long time, 30, 40 years,

76
00:05:05,970 --> 00:05:10,250
it was really a debt-based economy.

77
00:05:11,090 --> 00:05:15,320
-So a lot of people started producing?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
It's recording.

80
00:05:20,430 --> 00:05:23,260
Excuse me, I was talking to my colleague.

81
00:05:23,280 --> 00:05:26,070
-Are there many people on the farm?

82
00:05:26,850 --> 00:05:31,620
We are 16 adults, 27 children

83
00:05:31,650 --> 00:05:34,160
and at the moment we have a lot of friends visiting,

84
00:05:34,210 --> 00:05:36,000
volunteers, interns, ...

85
00:05:36,030 --> 00:05:38,930
In total we're about 50 people,

86
00:05:39,000 --> 00:05:40,370
maybe a little more.

87
00:05:40,410 --> 00:05:43,350
-And how big is the place?

88
00:05:44,940 --> 00:05:47,770
On the farm with the association

89
00:05:47,920 --> 00:05:54,140
we have 5 acres of land:

90
00:05:54,580 --> 00:05:57,940
4.5 acres for crops

91
00:05:57,950 --> 00:06:00,500
and half an acre for the buildings,

92
00:06:00,670 --> 00:06:02,980
the nurseries

93
00:06:03,070 --> 00:06:06,050
and the farmyard for chickens,

94
00:06:06,060 --> 00:06:07,640
goats and sheep.

95
00:06:07,710 --> 00:06:09,820
And this year we were able to

96
00:06:09,830 --> 00:06:13,490
rent an additional 70,000 square meters,

97
00:06:13,610 --> 00:06:16,600
about 17 acres,

98
00:06:16,700 --> 00:06:19,700
to multiply and expand our collection,

99
00:06:19,720 --> 00:06:21,350
especially our cereals.

100
00:06:22,280 --> 00:06:24,770
-But you don't live on the farm?

101
00:06:25,610 --> 00:06:27,510
Not quite.

102
00:06:27,520 --> 00:06:32,860
One of our colleagues and co-founders lives on the farm.

103
00:06:34,400 --> 00:06:36,320
We have a multi-purpose building

104
00:06:36,340 --> 00:06:39,780
which is a processing kitchen,

105
00:06:39,820 --> 00:06:43,090
kitchen for us, storage area, school

106
00:06:43,140 --> 00:06:44,350
and apartment,

107
00:06:44,360 --> 00:06:47,040
a rather long building.

108
00:06:47,060 --> 00:06:50,120
It's Walid who's living there with his family.

109
00:06:50,260 --> 00:06:52,440
Our other colleagues and friends

110
00:06:52,460 --> 00:06:54,260
live right across the street,

111
00:06:54,310 --> 00:06:56,530
and we're a bit further up the street,

112
00:06:56,550 --> 00:06:58,450
4 minutes on foot.

113
00:06:59,590 --> 00:07:02,290
-And you also produce some of

114
00:07:02,300 --> 00:07:04,400
what you consume yourselves?

115
00:07:04,740 --> 00:07:06,950
Yes!

116
00:07:07,160 --> 00:07:10,070
We buy very few things.

117
00:07:10,110 --> 00:07:13,770
Whatever we can produce, we don't buy it, we have it!

118
00:07:14,900 --> 00:07:17,980
There are things like rice,

119
00:07:18,010 --> 00:07:20,880
that are hard for us to produce,

120
00:07:20,910 --> 00:07:22,690
those kind of things.

121
00:07:22,920 --> 00:07:24,120
But apart from those…

122
00:07:24,130 --> 00:07:27,230
Last year we even managed to get some honey,

123
00:07:27,250 --> 00:07:30,520
but a predator attacked our bees,

124
00:07:30,560 --> 00:07:32,600
so we're down to 3 hives,

125
00:07:32,620 --> 00:07:34,570
out of 11.

126
00:07:36,830 --> 00:07:39,980
The idea is also to show that it is possible.

127
00:07:41,590 --> 00:07:46,920
-How about partners, bonds, the network

128
00:07:46,940 --> 00:07:50,600
that you have in Lebanon and also in Syria,

129
00:07:50,610 --> 00:07:54,570
how is that developing, how did it come about?

130
00:07:54,590 --> 00:07:56,450
Tell us about it!

131
00:07:57,100 --> 00:08:00,160
Basically, the idea was to first produce seeds

132
00:08:00,180 --> 00:08:03,170
and then teach those who are interested

133
00:08:03,190 --> 00:08:04,560
how to save their own seeds.

134
00:08:04,590 --> 00:08:08,450
so we don't have to keep producing them every year.

135
00:08:08,450 --> 00:08:11,030
It's really about learning autonomy

136
00:08:11,040 --> 00:08:12,560
and independence.

137
00:08:12,580 --> 00:08:16,380
So, after that, seed savers

138
00:08:16,400 --> 00:08:17,910
went to many places.

139
00:08:17,920 --> 00:08:19,710
Some returned to Syria

140
00:08:19,710 --> 00:08:22,260
to produce their own seeds in Syria.

141
00:08:22,280 --> 00:08:25,120
There are so many different situations.

142
00:08:25,150 --> 00:08:29,610
Our friends we've been following since the beginning

143
00:08:29,630 --> 00:08:32,020
are now completely autonomous,

144
00:08:32,040 --> 00:08:34,970
They are doing very well for themselves.

145
00:08:34,980 --> 00:08:36,910
Of course we follow up,

146
00:08:36,920 --> 00:08:40,350
if there is a need for exchange of information or experience:

147
00:08:40,360 --> 00:08:42,840
"we tried this", "they tested that".

148
00:08:42,920 --> 00:08:46,690
The idea is to have as many seed savers

149
00:08:46,690 --> 00:08:49,360
and seed banks as possible

150
00:08:49,390 --> 00:08:51,640
all over Lebanon

151
00:08:51,690 --> 00:08:55,600
and maybe even Syria one day, who knows.

152
00:08:57,460 --> 00:09:02,240
-Is the border still closed?

153
00:09:03,970 --> 00:09:06,550
Yes, it's closed

154
00:09:06,570 --> 00:09:09,540
and it's quite complicated to pass

155
00:09:09,610 --> 00:09:12,040
for people of certain nationalities.

156
00:09:12,050 --> 00:09:16,250
If you are not part of a diplomatic mission or the army

157
00:09:16,400 --> 00:09:17,990
it's still hard -

158
00:09:18,660 --> 00:09:20,860
it's better not to try.

159
00:09:21,700 --> 00:09:23,550
Better stay where you are.

160
00:09:23,580 --> 00:09:25,730
-About the trainings...

161
00:09:25,920 --> 00:09:30,670
You participated in the translation of DIYseeds -

162
00:09:34,330 --> 00:09:37,440
you translated the website?

163
00:09:38,750 --> 00:09:40,100
... the website in Arabic,

164
00:09:40,130 --> 00:09:44,680
and I reviewed the videos that were made by another team

165
00:09:44,820 --> 00:09:47,750
- a huge job I must say -

166
00:09:47,760 --> 00:09:50,090
it's crazy what they've done, it's so beautiful.

167
00:09:50,170 --> 00:09:52,080
And extremely practical.

168
00:09:52,120 --> 00:09:54,720
When you give a training

169
00:09:54,780 --> 00:09:57,840
where you try to explain how to extract seeds

170
00:09:57,870 --> 00:10:01,660
or how to keep safe distances

171
00:10:01,670 --> 00:10:03,360
to avoid cross-pollination,

172
00:10:03,380 --> 00:10:05,360
it's better with the pictures,

173
00:10:05,360 --> 00:10:08,140
the drawings, the video and the explanations.

174
00:10:08,140 --> 00:10:11,550
Not only do we show these videos during the trainings,

175
00:10:11,560 --> 00:10:13,470
but they are also accessible

176
00:10:13,480 --> 00:10:17,000
to people directly on their phones

177
00:10:17,020 --> 00:10:19,660
-when they have internet, but hey, it's accessible-,

178
00:10:19,680 --> 00:10:23,800
allowing people to manage very well on their own.

179
00:10:23,910 --> 00:10:25,680
It's great, it's perfect.

180
00:10:27,600 --> 00:10:30,770
-And it's easy for people in the region

181
00:10:31,240 --> 00:10:35,560
to have this information, to use it, to …

182
00:10:35,640 --> 00:10:36,480
Exactly!

183
00:10:36,540 --> 00:10:37,730
It makes things easier for them,

184
00:10:37,760 --> 00:10:40,500
because it's something quite light, simple.

185
00:10:40,520 --> 00:10:44,910
It's not a thick book that you have to carry around.

186
00:10:44,960 --> 00:10:47,940
There are a lot of farmers who can't read,

187
00:10:48,000 --> 00:10:50,220
but they understand what they hear.

188
00:10:50,260 --> 00:10:52,970
There are also the drawings and the video itself,

189
00:10:53,040 --> 00:10:55,570
which illustrate the words much better,

190
00:10:55,580 --> 00:10:58,770
it's much more accessible and simpler,

191
00:10:58,800 --> 00:11:01,830
as a tutorial it's easier to follow

192
00:11:01,870 --> 00:11:05,270
than to take your thick book and decipher.

193
00:11:06,000 --> 00:11:11,350
-As the seeds you produce are non-hybrid,

194
00:11:11,380 --> 00:11:14,580
natural, reproducible seeds,

195
00:11:14,660 --> 00:11:18,030
everyone who has these seeds

196
00:11:18,160 --> 00:11:21,380
and access to the videos can quickly recover autonomy.

197
00:11:21,400 --> 00:11:26,560
Can you tell us how, and how quickly?

198
00:11:26,580 --> 00:11:27,630
How quickly?

199
00:11:27,650 --> 00:11:30,530
That depends on the access to land,

200
00:11:30,560 --> 00:11:32,160
it depends on the access to water,

201
00:11:32,180 --> 00:11:34,310
it depends on the security of the area.

202
00:11:34,320 --> 00:11:37,320
We have friends who have had to move several times

203
00:11:37,390 --> 00:11:39,260
from their area in Syria.

204
00:11:39,300 --> 00:11:41,170
Every time they settled down

205
00:11:41,180 --> 00:11:42,830
there were bombings,

206
00:11:42,840 --> 00:11:44,810
so they had to move again and again.

207
00:11:44,960 --> 00:11:47,240
The context itself is complicated.

208
00:11:47,320 --> 00:11:51,670
But when you have the basic tools,

209
00:11:51,710 --> 00:11:54,660
you can manage wherever you end up.

210
00:11:54,690 --> 00:11:57,110
You have your seeds, you know how to plant them,

211
00:11:57,130 --> 00:12:00,260
you know how to reproduce them, extract them,

212
00:12:00,290 --> 00:12:01,990
save them, preserve them, etc.

213
00:12:02,020 --> 00:12:04,980
You are a free man or woman!

214
00:12:07,140 --> 00:12:08,050
That's it.

215
00:12:08,190 --> 00:12:12,120
-So you see people who, in one season,

216
00:12:12,170 --> 00:12:15,180
learned to produce seeds -

217
00:12:15,580 --> 00:12:19,470
so now they have their own seeds?

218
00:12:19,520 --> 00:12:20,510
Exactly.

219
00:12:20,530 --> 00:12:23,240
Most of the people we work with

220
00:12:23,290 --> 00:12:25,350
were raised with agriculture,

221
00:12:25,370 --> 00:12:26,840
have a background in farming,

222
00:12:26,850 --> 00:12:29,000
or have worked in agriculture,

223
00:12:29,020 --> 00:12:30,440
often conventional farming.

224
00:12:30,460 --> 00:12:34,350
It's easier for them to make the transition.

225
00:12:36,550 --> 00:12:38,500
In the past they had to

226
00:12:38,520 --> 00:12:39,970
buy seeds

227
00:12:39,970 --> 00:12:42,450
and the second they learned

228
00:12:42,470 --> 00:12:45,010
that their fate was in their hands,

229
00:12:45,260 --> 00:12:47,100
because they just needed

230
00:12:47,120 --> 00:12:50,730
the right tools,

231
00:12:50,880 --> 00:12:53,890
after a season or two they have their own seeds,

232
00:12:53,930 --> 00:12:55,040
and we exchange.

233
00:12:55,080 --> 00:12:56,800
We work together over the long term,

234
00:12:56,820 --> 00:13:00,570
for example someone grows a variety,

235
00:13:00,580 --> 00:13:03,410
someone grows another one, we produce a third one…

236
00:13:03,423 --> 00:13:08,680
to have as much diversity as possible and that's it,

237
00:13:08,720 --> 00:13:10,680
it's starting to work.

238
00:13:11,760 --> 00:13:16,280
-And you, how did you get involved in all this?

239
00:13:18,426 --> 00:13:20,280
Really by chance.

240
00:13:20,330 --> 00:13:26,600
I was living in Beirut and my roommate was at university with Ferdi,

241
00:13:26,693 --> 00:13:30,613
who is one of the co-founders of the association and the project.

242
00:13:32,320 --> 00:13:35,253
So when Ferdi and Lara returned to Lebanon

243
00:13:35,293 --> 00:13:38,253
to see how to start the seeds project,

244
00:13:38,290 --> 00:13:41,270
how to produce, where to do all this,

245
00:13:41,308 --> 00:13:43,228
what needed to be done,

246
00:13:43,242 --> 00:13:44,927
they stayed with us

247
00:13:44,936 --> 00:13:46,592
for about a month,

248
00:13:46,602 --> 00:13:48,545
we got along immediately and that was it.

249
00:13:49,727 --> 00:13:51,303
As I said before:

250
00:13:51,336 --> 00:13:54,174
I consciously allowed myself to be pulled on board!

251
00:13:54,988 --> 00:13:56,898
-And you changed your life?

252
00:13:57,068 --> 00:13:59,101
What did you do before?

253
00:14:00,112 --> 00:14:03,303
Just before I was working in communication

254
00:14:03,336 --> 00:14:04,823
for a university.

255
00:14:04,851 --> 00:14:06,960
But I'm actually a librarian,

256
00:14:09,520 --> 00:14:12,640
I studied psychology, literature,

257
00:14:12,672 --> 00:14:15,868
nothing to do with seeds!

258
00:14:19,745 --> 00:14:21,520
But it helps to translate,

259
00:14:21,543 --> 00:14:22,922
so it's perfect!

260
00:14:23,275 --> 00:14:24,992
-And you want to continue?

261
00:14:25,920 --> 00:14:27,637
Yes!

262
00:14:27,661 --> 00:14:29,863
As we were saying earlier:

263
00:14:29,890 --> 00:14:33,002
we're in the middle of an emergency,

264
00:14:34,164 --> 00:14:39,185
I think this work is really essential,

265
00:14:40,014 --> 00:14:41,477
it's necessary.

266
00:14:41,515 --> 00:14:47,298
And it's also rewarding on many levels:

267
00:14:47,310 --> 00:14:50,555
on a social level, on a political level too.

268
00:14:50,597 --> 00:14:53,050
What we do is extremely important

269
00:14:53,072 --> 00:14:54,823
and beautiful at the same time.

270
00:14:55,247 --> 00:14:58,211
I don't think I'll stop anytime soon!

271
00:14:58,418 --> 00:15:01,797
-Great! Thanks for the interview.

272
00:15:01,960 --> 00:15:03,722
You're welcome, you're welcome!

273
00:15:03,755 --> 00:15:06,290
It was cool talking to you again too.

274
00:15:07,620 --> 00:15:08,936
-I hope we'll see each other again.

275
00:15:08,960 --> 00:15:11,134
We're waiting for you on the farm!

276
00:15:11,181 --> 00:15:15,811
-I would love to :-) I can't promise anything but…

277
00:15:17,463 --> 00:15:20,560
Just let us know you're coming, whenever you want,

278
00:15:20,583 --> 00:15:22,672
and "ahla wa sahla" (welcome!)

279
00:15:25,251 --> 00:15:26,409
-Thank you very much!

280
00:15:26,432 --> 00:15:27,091
Great!
