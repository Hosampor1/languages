# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: abc-selection\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "The selection process enables you to gradually adapt the plant to the environment, to your needs and wishes."
msgstr ""

msgid "It is important to pay particular attention to plants grown for their seeds, as they will produce the future generations of vegetables and fruits. It is therefore necessary to choose them well and to observe them throughout their development."
msgstr ""

msgid "Selection carried out only on the basis of the fruit will not reflect all of the characteristics linked to the development of plants. Selection criteria must be precisely defined, as they will determine the choice of plants for seed production."
msgstr ""

msgid "You should first of all consider the criteria specific to the variety, such as resistance to diseases, yield, precocity, as well as more subjective criteria such as taste and appearance. The capacity of plants to adapt to their environment and to cultivation methods should also be taken into account."
msgstr ""

msgid "The key thing is to know from the start what aspects you want to give priority to, because over generations the variety will evolve and certain characteristics will become less present or dominant."
msgstr ""

msgid "Sometimes it is not wise to carry out a selection process. This is the case when there are very few seeds left of a rare variety facing the risk of extinction, as there will not be enough plants produced the first year to enable you to carry out selection."
msgstr ""

msgid "You can also decide to preserve the variety as it is, without a selection process. In this case you take all of the elements of the variety, even if there is a strong disparity, in order to encourage genetic diversity. This creates a strong capacity for adaptation and provides a reservoir of potentialities, which could provide the basis of future selection."
msgstr ""

msgid "It is important to clearly mark the plants grown for seed production so as to distinguish them from those grown for their vegetables. Drawing up a plan of the garden indicating the location of each variety will help you find them if the labels have disappeared."
msgstr ""

msgid "The complete cycle of plants through to their seeds is often longer than that of those grown for their vegetables. For example, you can eat lettuce after two or three months, but its full life cycle through to seed harvest is five to six months. There exist many biennial plants, such as the carrot, which flower and produce their seeds in the second year of growth. The best of all is to reserve a small corner of the garden specifically for seed production."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Selection of plants grown for their seeds"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "The selection process enables you to gradually adapt the plant to the environment, to your needs and wishes. Video explanations."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "selection, seeds, seed carrier, environment, adaptation, disappearance, explanations, functioning, learn, ABC, video"
msgstr ""
