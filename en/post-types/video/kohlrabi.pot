# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: kohlrabi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "Kohlrabi is a member of the Brassicaceae family, the Brassica oleracea species and the caulorapa gongylodes subspecies."
msgstr ""

msgid "The brassica oleracea species also includes broccoli, cabbage, Brussels sprouts, kale, cauliflower and the Savoy Cabbage."
msgstr ""

msgid "Kohlrabi is a biennial plant grown for its swollen stem."
msgstr ""

msgid "The plants produce very few leaves and its flesh remains tender."
msgstr ""

msgid "There are different varieties distinguished by the size and colour of the stem."
msgstr ""

msgid "There are early varieties and varieties for storage."
msgstr ""

msgid "In the first year of cultivation, plants of storage varieties grown for seed are cultivated in the same way as those for consumption."
msgstr ""

msgid "In the first year, they form swollen stems that have to overwinter and then flower the following year."
msgstr ""

msgid "Stems that are over-developed or split at the end of autumn will be difficult to overwinter. In most regions, you should therefore sow both early and storage varieties in June or July."
msgstr ""

msgid "To ensure good genetic diversity, you should select 30 kohlrabi plants in autumn in order to obtain 10 to 15 plants in spring."
msgstr ""

msgid "Producing kohlrabi seed requires healthy plants that have been observed throughout the growth period to identify the characteristics of the variety: the quality of the stem, vigour and rapid growth, resistance to diseases, good storage capacity, precocity, resistance to cold."
msgstr ""

msgid "The plants intended for seed production can be stored in a cellar; you should cut off the side leaves but leave the central ones."
msgstr ""

msgid "The plants are then placed in sand or in containers."
msgstr ""

msgid "During winter kohlrabi is less likely to rot than cabbages."
msgstr ""

msgid "In regions with a mild climate, kohlrabi can remain in the ground since it can tolerate frosts as low as -7° C. A dry winter is the best and stems with less water content are more resistant to the rigours of winter."
msgstr ""

msgid "In spring when the danger of a heavy frost is past, take out the kohlrabi from their place of storage and replant them in the ground, burying 2/3 of their volume."
msgstr ""

msgid "The kohlrabi will grow flower stalks and then flower."
msgstr ""

msgid "To avoid them falling, it is sometimes necessary to support the flower stalks with stakes."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your kohlrabi seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own kohlrabi seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, kohlrabi, step by step, explanations"
msgstr ""
