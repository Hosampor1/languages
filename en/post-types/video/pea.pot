# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: pea\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "Peas belong to the Fabaceae family and to the Pisum sativum species. They are annual plants. There are many varieties of peas:"
msgstr ""

msgid "Peas which must be shelled and whose seeds are round and smooth. These are hardy varieties that mature early and resist to cold."
msgstr ""

msgid "Peas which must also be shelled but whose seeds are wrinkled. They are less suitable for early sowing, but they are more resistant to hot temperatures."
msgstr ""

msgid "Edible podded peas. You can eat the whole pods when they are young, before the seeds have developed."
msgstr ""

msgid "Among these pea varieties, there are certain climbing varieties that grow to over 70 centimeters. It is important to stake them."
msgstr ""

msgid "There are also dwarf peas that grow to 45 to 70 centimeters high. They don't generally need staking."
msgstr ""

msgid "In the Pisum sativum species there are also fodder peas."
msgstr ""

msgid "Pea flowers are hermaphrodite and self-fertilising, meaning they have the male and female organs within the same flower and that they are compatible."
msgstr ""

msgid "Cross pollination by insects may however occur between different varieties. The risk depends on the variety and the environment, whether there are natural barriers or not."
msgstr ""

msgid "To avoid cross-pollination, grow different varieties of pea 15 meters apart. A few meters are enough if there is a natural barrier such as a hedge between them."
msgstr ""

msgid "When you wish to grow several varieties in the same garden you can isolate one variety under a net cage. Make sure you put the cage in place before flowering begins."
msgstr ""

msgid "Peas grown for seeds are grown in the same way as those for consumption."
msgstr ""

msgid "Some peas can be sown before the winter, but usually peas are sown early in the spring. You should not sow peas too late as pea flowers cannot be fertilized at temperatures above 30°c."
msgstr ""

msgid "Grow at least 50 plants to ensure good genetic diversity and a better selection."
msgstr ""

msgid "During growth, select the most beautiful, healthy and productive plants, and remove the others."
msgstr ""

msgid "It is better to reserve part of the crop for seed production from which you should not harvest any pods for consumption. You should let all seeds mature fully. In this way you will preserve the precocity of the variety."
msgstr ""

msgid "To harvest the seeds, all you have to do is let the plants dry in the garden."
msgstr ""

msgid "If necessary, you can complete drying in a shed."
msgstr ""

msgid "To make sure that the seeds are dry, bite one gently. If this leaves no mark, then they are fully dry."
msgstr ""

msgid "The shelling of the pods can either be done pod by pod, or you can beat them with a stick or tread on them."
msgstr ""

msgid "After this, remove the large debris by using a sieve. The larger waste should be removed by hand and the smaller chaff will pass through the sieve."
msgstr ""

msgid "You must then winnow the seeds to remove the very last chaff by blowing on them yourself or by using a ventilator or a small air-compressor."
msgstr ""

msgid "It is important to put a label with the name of the variety and species, as well as the year, inside the package, as writing on the outside often rubs off."
msgstr ""

msgid "Pea seeds are often infested by pea weavils (bruchus pisurum), small insects that lay their eggs under the skin of seeds while the plants are growing in the garden."
msgstr ""

msgid "An easy way to get rid of them is to leave the seeds in the freezer for a few days."
msgstr ""

msgid "Pea seeds have a germination capacity of 3 to 8 years."
msgstr ""

msgid "This can be extended by storing the seeds in a freezer."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your pea seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own pea seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, pea, step by step, explanations"
msgstr ""
